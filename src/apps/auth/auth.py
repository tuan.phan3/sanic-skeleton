from datetime import datetime, timezone
from sanic.response import json
from sanic import Blueprint

from .utils import firebase_authorized
from src.apps.user.models import Users

auth_bp = Blueprint('auth', url_prefix='auth', version=1)


@auth_bp.route('/jwt_verify')
@firebase_authorized()
async def jwt_verify(request, **kwargs):
    fb_user = kwargs.get("fb_user", None)
    if not fb_user:
        raise RuntimeError()

    request_user = await Users.query.where(Users.fb_uid == fb_user.uid).gino.first()
    user_id = None
    if not request_user:
        created_user = await Users.create(fb_uid=fb_user.uid)
        user_id = created_user.id
    else:
        user_id = request_user.id

    if fb_user.email == "tuan.phan@sparkling.vn":
        from firebase_admin import auth
        auth.set_custom_user_claims(fb_user.uid, {'is_admin': True})

    last_login_at = fb_user.user_metadata.last_sign_in_timestamp
    last_login_at = datetime.fromtimestamp(last_login_at/1000)

    created_at = fb_user.user_metadata.creation_timestamp
    created_at = datetime.fromtimestamp(created_at/1000)

    res = dict(
        id=str(user_id),
        fb_uid=fb_user.uid,
        email=fb_user.email,
        display_name=fb_user.display_name,
        photo_url=fb_user.photo_url,
        email_verified=fb_user.disabled,
        is_admin=fb_user.custom_claims.get("is_admin", False),
        disabled=fb_user.disabled,
        last_login_at=last_login_at.replace(tzinfo=timezone.utc).isoformat(),
        created_at=created_at.replace(tzinfo=timezone.utc).isoformat(),
        phone_number=fb_user.phone_number
    )
    return json(res)
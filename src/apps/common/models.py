import uuid
from sqlalchemy.dialects.postgresql import UUID

from src.core.extensions import db
print("dfsfdfkndskfndkl")


class UUIDModel(db.Model):
    """Base model for primary as UUIDv4. All the models which use uuid should extend it."""

    __abstract__ = True

    id = db.Column(UUID(as_uuid=False), unique=True, nullable=False, primary_key=True, default=str(uuid.uuid4()))
import argparse

# Command line parser options & setup default values
COMMAND_RUNSERVER = 'runserver'
COMMAND_USERMNGT = 'usermngt'

parent_parser = argparse.ArgumentParser(description='Sanic command line tool.')
subparsers = parent_parser.add_subparsers(
    title='Sanic commands',
    # description='Valid subcommands',
    help='additional help',
    dest='sanic_command'
)

runserver_parser = subparsers.add_parser(COMMAND_RUNSERVER, description='Run sanic server.')
runserver_parser.add_argument('--host', help='Setup host ip to listen up, default to 0.0.0.0', default='0.0.0.0')
runserver_parser.add_argument('--port', help='Setup port to attach, default to 8000', default='8000')
runserver_parser.add_argument('--workers', help='Setup workers to run, default to 1', type=int, default=1)
runserver_parser.add_argument('--debug', help='Enable or disable debugging', action='store_true')
runserver_parser.add_argument('--accesslog', help='Enable or disable access log', action='store_true')

usermngt_parser = subparsers.add_parser(COMMAND_USERMNGT, description='User management.')
usermngt_subparsers = usermngt_parser.add_subparsers(
    title='User management commands',
    # description='valid subcommands',
    help='additional help',
    dest='usermngt_command'
)
promote_parser = usermngt_subparsers.add_parser("promote", description='Promote.')
promote_parser.add_argument('user_email', metavar='USER_EMAIL', help='Promote user to admin', default=None)


args = parent_parser.parse_args()


# Running sanic, we need to make sure directly run by interpreter
# ref: http://sanic.readthedocs.io/en/latest/sanic/deploying.html#running-via-command
if __name__ == '__main__':
    print(args)
    if args.sanic_command == COMMAND_RUNSERVER:
        # print("Run Sanic app with config name: {}".format(config_name))
        print("Run sanic server ...")
        from src.entry import app
        app.run(
            host=args.host,
            port=args.port,
            workers=args.workers,
            debug=args.debug,
            access_log=args.accesslog
        )

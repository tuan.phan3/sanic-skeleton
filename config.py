import dotenv
from os import environ
from os.path import join, dirname
from distutils.util import strtobool

class Config(object):

    def __init__(self, filename='.env'):
        envpath = join(dirname(__file__), filename)
        dotenv.load_dotenv(envpath)

        self.SECRET_KEY = environ.get('SECRET_KEY')
        self.DB_HOST = environ.get('DB_HOST', "localhost")
        self.DB_PORT = environ.get('DB_PORT', 5432)
        self.DB_USER = environ.get('DB_USER')
        self.DB_PASSWORD = environ.get('DB_PASSWORD')
        self.DB_DATABASE = environ.get('DB_DATABASE')
        self.DB_USE_CONNECTION_FOR_REQUEST = bool(strtobool(environ.get('DB_USE_CONNECTION_FOR_REQUEST', True)))


class DevConfig(Config):

    def __init__(self, filename='.env'):
        super().__init__()

        self.SECRET_KEY = 'you-are-dev-safe-now'


class ProdConfig(Config):

    def __init__(self, filename='.env'):
        super().__init__()

        self.SECRET_KEY = 'you-are-prod-safe-now'


class TestingConfig(Config):

    def __init__(self, filename='test.env'):
        super().__init__()

        self.SECRET_KEY = 'you-are-test-safe-now'
        self.DB_HOST = environ.get('DB_HOST', "localhost")
        self.DB_PORT = environ.get('DB_PORT', 5432)
        self.DB_USER = environ.get('DB_USER', "test")
        self.DB_PASSWORD = environ.get('DB_PASSWORD', "test")
        self.DB_DATABASE = environ.get('DB_DATABASE', "test")


app_config = {
    'dev': DevConfig,
    'prod': ProdConfig,
    'testing': TestingConfig
}
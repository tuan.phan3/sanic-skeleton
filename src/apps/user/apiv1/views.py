from sanic_restplus import Resource, fields, Namespace
# from sanic_restplus import reqparse
from src.core.extensions import apiv1
from src.core.extensions import db

from src.apps.user.models import Users
from .serializers import user_detail
from .serializers import users_list


users_ns = apiv1.namespace('users', description='User management API')


pagination_arguments = users_ns.parser()
pagination_arguments.add_argument('page', type=int, required=False)
pagination_arguments.add_argument('per_page', type=int, required=False,
                                  choices=[5, 10, 20, 30, 40, 50], default=10)


class Pagination:

    def __init__(self, request, req_args):
        pass

    # def paginate_query(self, )


def paginate_query(query, per_page, page_num):
    if per_page == 0:
        return query

    query = query.limit(per_page)
    if page_num:
        query = query.offset((page_num - 1) * per_page)

    return query


@users_ns.route('')
class UsersView(Resource):
    '''Shows a list of all todos, and lets you POST to add new tasks'''

    model = Users
    query = Users.query

    def paginate_query(self, query, per_page, page_num):
        if per_page == 0:
            return query

        query = query.limit(per_page)
        if page_num:
            query = query.offset((page_num - 1) * per_page)

        return query

    @users_ns.doc('list_users')
    @users_ns.expect(pagination_arguments, validate=True)
    @users_ns.marshal_with(users_list)
    async def get(self, request, context, **kwargs):
        '''List all Users in system'''
        req_context = context['request'][id(request)]

        total_user = await db.func.count(self.model.id).gino.scalar()
        req_args = pagination_arguments.parse_args(request, req_context)

        per_page = request.app.config.get('PER_PAGE', 10)
        if req_args.get('per_page', None):
            per_page = req_args["per_page"]

        page_num = req_args.get('page', 1)
        query = self.paginate_query(self.query, per_page, page_num)
        collection = await query.gino.all()
        print(collection)

        count_page = 1
        if total_user > per_page:
            div_ret = total_user//per_page
            mod_ret = total_user%per_page

            count_page = div_ret
            if mod_ret != 0:
                count_page += 1

        # TODO: generate template
        res = dict(
            _links=dict(
                next='next',
                prev='prev',
                first='first',
                last='last'
            ),
            meta=dict(
                per_page=per_page,
                total_result=total_user,
                page=page_num,
                count_page=count_page
            ),
            results=[r.serialized for r in collection]
        )
        print(request.uri_template)

        return res

    @users_ns.doc('create_user')
    @users_ns.expect(user_detail)
    @users_ns.marshal_with(user_detail, code=201)
    async def post(self, request, context, **kwargs):
        '''Create a new User into system'''
        return DAO.create(request.json), 201
from datetime import datetime, timezone
from firebase_admin import auth

from src.core.extensions import db
from src.apps.common.models import UUIDModel


class Users(UUIDModel):
    __tablename__ = "users"

    fb_uid = db.Column(db.String, unique=True, index=True)

    def __repr__(self):
        return "Users [{}]".format(self.fb_uid)

    @property
    def serialized(self):
        fb_user = auth.get_user(self.fb_uid)

        last_login_at = fb_user.user_metadata.last_sign_in_timestamp
        last_login_at = datetime.fromtimestamp(last_login_at/1000)

        created_at = fb_user.user_metadata.creation_timestamp
        created_at = datetime.fromtimestamp(created_at/1000)

        return dict(
            id=str(self.id),
            fb_uid=self.fb_uid,
            email=fb_user.email,
            display_name=fb_user.display_name,
            photo_url=fb_user.photo_url,
            email_verified=fb_user.disabled,
            is_admin=fb_user.custom_claims.get("is_admin", False),
            disabled=fb_user.disabled,
            last_login_at=last_login_at.replace(tzinfo=timezone.utc).isoformat(),
            created_at=created_at.replace(tzinfo=timezone.utc).isoformat(),
            phone_number=fb_user.phone_number
        )
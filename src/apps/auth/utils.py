from sanic.response import json
from http import HTTPStatus
from functools import wraps
from firebase_admin import auth

from .errors import AuthErrorCode
from src.core.helpers import jsonapi


AUTHORIZATION_HEADER_PREFIX = "Bearer"
USER_UNAUTHORIZED = 'User is not Authorized'


def format_auth_error(code, detail):
    return jsonapi.format_error(title=USER_UNAUTHORIZED, detail=detail, status=HTTPStatus.FORBIDDEN, code=code)


def check_request_for_authorization_status(request):
    # Note: Define your check, for instance cookie, session.
    flag = False
    detail = ''
    uid = None
    value = request.headers.get('authorization', None)
    if AUTHORIZATION_HEADER_PREFIX not in value:
        detail = "Missing {} in authorization value.".format(AUTHORIZATION_HEADER_PREFIX)
    else:
        try:
            jwt_token = value.split(" ")[1]
        except Exception as e:
            detail = str(e)

        try:
            decoded_token = auth.verify_id_token(jwt_token)
            uid = decoded_token['uid']
            flag = True
        except Exception as e:
            detail = str(e)

    return flag, uid, detail


def firebase_authorized():
    def decorator(f):
        @wraps(f)
        async def decorated_function(request, *args, **kwargs):
            # run some method that checks the request
            # for the client's authorization status
            is_valid_jwt, uid, detail = check_request_for_authorization_status(request)

            errors = []
            if is_valid_jwt:
                # the user jwt is valid.
                # run the handler method and return the response
                fb_user = auth.get_user(uid)

                if not fb_user.email_verified:
                    errors.append(format_auth_error(code=AuthErrorCode.EMAIL_UNVERIFIED, detail="Email is not verified."))

                if fb_user.disabled:
                    errors.append(format_auth_error(code=AuthErrorCode.USER_DISABLED, detail="This account is disabled."))
            else:
                # the user is not authorized.
                errors.append(format_auth_error(code=AuthErrorCode.INVALID_JWT, detail=detail))

            if errors:
                return json(jsonapi.return_an_error(*errors), status=HTTPStatus.FORBIDDEN)
            else:
                response = await f(request, *args, fb_user_uid=uid, fb_user=fb_user, **kwargs)
                return response

        return decorated_function
    return decorator
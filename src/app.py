import firebase_admin
from sanic import Sanic

from config import app_config


def create_app(config_name='prod'):
    # Configure Sanic apps
    app = Sanic(__name__)
    app.config.from_object(app_config[config_name]())

    # Init all extensions
    _init_core_extensions(app)

    # Register API Endpoints
    _init_blueprint(app)

    _init_firebase_admin(app)

    return app


def _init_core_extensions(app):

    # Init database
    from src.core.extensions import init_db
    init_db(app)

    from src.core.extensions import init_api
    init_api(app)

    # from src.core.extensions import api
    # from src.apps.user.views import ns
    # api.add_namespace(ns)



def _init_blueprint(app):
    from sanic import Blueprint

    # Import base blueprint
    from src.apps.ping import blueprint as ping_app
    app.blueprint(ping_app, url_prefix='/ping')

    # from src.core.extensions.middlewares import blueprint as ext_middlewares
    # app.blueprint(ext_middlewares)

    from src.apps.auth import auth_bp

    apiv1 = Blueprint.group(auth_bp)
    app.blueprint(apiv1)


def _init_firebase_admin(app):

    print("Init Firebase Admin")
    fb_admin_app = firebase_admin.initialize_app()
    from firebase_admin import auth
    for user in auth.list_users().iterate_all():
        print(user.__dict__)
    return fb_admin_app
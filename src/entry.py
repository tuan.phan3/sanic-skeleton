from os import environ
from .app import create_app


app = application = create_app(environ.get("SANIC_CONFIG_ENV", "prod"))

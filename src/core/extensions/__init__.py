from gino.ext.sanic import Gino
from sanic_restplus import Api

db = Gino()
apiv1 = Api(version='1.0', title='TodoMVC API',
          description='A simple TodoMVC API', doc='/v1/swagger-ui', prefix='v1')

def init_db(app):
    from src.apps.common import models
    from src.apps.user import models

    # app.config.setdefault('DB_DATABASE', 'pie')
    print(app.config)
    db.init_app(app)


def init_api(app):
    from spf import SanicPluginsFramework
    from sanic_restplus.restplus import restplus

    spf = SanicPluginsFramework(app)
    rest_assoc = spf.register_plugin(restplus)
    rest_assoc.api(apiv1)
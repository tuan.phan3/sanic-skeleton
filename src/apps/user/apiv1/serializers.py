from sanic_restplus import fields
from src.core.extensions import apiv1

meta = apiv1.model('Meta', {
    'per_page': fields.Integer(description='Number of items per page of results'),
    'total_result': fields.Integer(description='Total number of results'),
    'count_page': fields.Integer(description='Total number of pages'),
    'page': fields.Integer(description='Number of this page of results'),
})

links = apiv1.model('Links', {
    'next': fields.String(description='The next page of results'),
    'prev': fields.String(description='The previous page of results'),
    'first': fields.String(description='The first page of results'),
    'last': fields.String(description='The last page of results')
})

collection = apiv1.model('Collection', {
    '_links': fields.Nested(links),
    'meta': fields.Nested(meta)
})
{'_data': {'localId': 'Cgtf9xg8u1T3U97iGpcAPbTSGSe2', 'email': 'tuan.phan@sparkling.vn', 'displayName': 'Tuấn Phan Hữu', 'photoUrl': 'https://lh3.googleusercontent.com/a-/AAuE7mC0L9MSmYHLdpNc50ilH_S9WvvUK1WcHIamXyzN', 'passwordHash': 'M0yNWdRkuVGXYM56t5pvNxd1UXYJMb9Lgz3nUNZCv0i01HRnShBClMiAhIKSYXqsXovfSA33jz5ztEU30aCANQ==', 'salt': '2LzBQ-FsP3HAyA==', 'version': 0, 'emailVerified': True, 'passwordUpdatedAt': 1570523420432, 'providerUserInfo': [{'providerId': 'google.com', 'displayName': 'Tuấn Phan Hữu', 'photoUrl': 'https://lh3.googleusercontent.com/a-/AAuE7mC0L9MSmYHLdpNc50ilH_S9WvvUK1WcHIamXyzN', 'federatedId': '117050920091715022877', 'email': 'tuan.phan@sparkling.vn', 'rawId': '117050920091715022877'}, {'providerId': 'password', 'displayName': 'Tuấn Phan Hữu', 'photoUrl': 'https://lh3.googleusercontent.com/a-/AAuE7mC0L9MSmYHLdpNc50ilH_S9WvvUK1WcHIamXyzN', 'federatedId': 'tuan.phan@sparkling.vn', 'email': 'tuan.phan@sparkling.vn', 'rawId': 'tuan.phan@sparkling.vn'}], 'validSince': '1570523420', 'disabled': False, 'lastLoginAt': '1571127288720', 'createdAt': '1570523420432', 'customAttributes': '{"is_admin": true}', 'lastRefreshAt': '2019-10-16T07:45:49.189Z'}}


user_detail = apiv1.model('UserDetail', {
    'id': fields.String(description='The user id in system.'),
    'fb_uid': fields.String(description='The user id in firebase.'),
    'email': fields.String(description="The user's email in firebase."),
    'display_name': fields.String(description="The user's display name in firebase."),
    'photo_url': fields.String(description="The user's avatar."),
    'email_verified': fields.Boolean(description="Determine email is verified or not."),
    'is_admin': fields.Boolean(description="Determine user role, admin or normal user."),
    'disabled': fields.Boolean(description="Is this account disabled or not."),
    'last_login_at': fields.String(description="Last login time."),
    'created_at': fields.String(description="Account created at time."),
    'last_refresh_at': fields.String(description="Last jwt token refresh time."),
    'phone_number': fields.String(description="The user's phone number.")
})


users_list = apiv1.inherit('Users', collection, {
    'results': fields.List(fields.Nested(user_detail))
})
